#ifndef VERSION_H
#define VERSION_H

extern const char * build_git_sha;
extern const char * build_git_time;
extern const char * build_git_author;
extern const char * build_git_status;

#endif /* VERSION_H */