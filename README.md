# Embedded Header

The example project shows utilization of free tools to acheive custom header which will be updated to the latest build.

This is example with the Segger Embedded Studio and Nordic Semiconductors project, but the same approach can be used in 
any executable project (or even better to any makefile based project).

This project is a demo version.

Full version support display of e.g.
- compilation date
- branch name
- changes status
- tag version
- Support of Git or Mercurial
- Support of Linux or Windows
Please conntact author for further details: pl.krzyzanowski@gmail.com